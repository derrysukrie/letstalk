import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, ScrollView, View, Image } from 'react-native';
import { Container, Content, Body, Button, Left, Right, Icon, Card, CardItem,Thumbnail, List, ListItem  } from 'native-base';
import AppHeader from '../../components/header/AppHeader';

export default class Home extends Component {
    render() {
        return (
            <Container style={{backgroundColor:'#97cdd0'}}>
                <AppHeader />
                   <Content>
                       <Container style={{backgroundColor:'#000000', height:200}}>
                       </Container>
                        <Text style={{padding:30, fontWeight:'500'}}>Hey Kamu! Mau Cerita?</Text>
                       <Left/>
                       <Body>
                       <Button 
                            style={{height:70, width:300, justifyContent:'center'}}
                            light>
                                <Icon style={{paddingLeft:180}} name='arrow-dropright'/>
                            </Button>
                       </Body>
                       <Right />
                   </Content>
                   
            </Container>

           /* <Container>
                <AppHeader />
            <Content>
                <Container style={styles.color}>
                    <Text>belom ada gamabr</Text>
                </Container>
                <Container style={styles.colorBody}>
                <Text style={styles.textStyle}>Hey Kamu! Mau cerita?</Text>
                 <Content>
                     <Left />
                     <Body>
                    <Button 
                        style={{height:60, width:300,alignContent:'center', justifyContent:'center'}}
                        light>
                        <Text style={{justifyContent:'center'}}>Cerita apapun keluh kesah kamu di sini</Text>
                    </Button>
                     </Body>
                     <Right/>
                     <ScrollView
                        scrollEventThrottle={16}
                    >
                        <View style={{flex:1, paddingTop:30}}>
                            <Text style={{fontSize: 20, paddingHorizontal: 20}}>
                                awdawdawda 
                            </Text>
                            <View style={{height:130, marginTop: 20}}>
                                    <ScrollView>
                                        <View style={{height: 150, width:200, marginLeft: 20, borderWidth:0.5,borderColor:'#fff'}}>
                                            <View style={{flex:2}}>
                                                <Image source={require('../../assets/img/yellow.jpg')}
                                                style={{flex:1, width:null, height:null,resizeMode:'cover'}}
                                                 />
                                            </View>
                                            <View style={{flex:1, alignItems: 'center'}}>
                                                <Text style={{padding:5}}>awdawdawwawaawdawdawdaw</Text>
                                            </View>
                                        </View>
                                    </ScrollView>
                            </View>
                        </View>
                    </ScrollView>
                 </Content>  
               </Container>
            </Content>
        </Container>
        */
        );
    }
}

const styles = StyleSheet.create({
    color: {
        backgroundColor: '#000',
        height:200
    },
    colorBody: {
        backgroundColor: '#97cdd0'
    },
    listMenuColor: {
        color: 'black'
    },
    textStyle: {
        
        padding: 30,
        fontWeight: '700',
        color: '#0f5652',
        fontSize: 16
    },
    cardStyle: {
        padding:20,
        paddingTop: 100
    }
});