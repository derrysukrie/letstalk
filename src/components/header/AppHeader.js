import React from 'react';
import {StyleSheet, Image} from 'react-native'
import { Text, Header, Left, Button, Icon, Body, Right } from 'native-base';

class AppHeader extends React.Component{
    render(){
        return(
            <Header>
                <Left>
                    <Button transparent>
                        <Icon 
                            name='ios-menu'
                            style={styles.listMenuColor}
                        />
                    </Button>
                </Left>
                <Body>
                    <Image
                        source={require('../../assets/img/headerlogo.png')} />
                </Body>
                <Right />
            </Header>
        );
    }
}
export default AppHeader;

const styles = StyleSheet.create({
    color: {
        backgroundColor: '#97cdd0'
    },
    listMenuColor: {
        color: 'black'
    },
    colors:{
        color: '#fff'
    },
    textInput1: {
        borderRadius: 3,
        backgroundColor: '#fff',
        marginTop: 10,
        margin: 10,
        paddingTop: 10
    },
    textInput2: {
        borderRadius: 3,
        backgroundColor: '#fff',
        fontSize: 11,
        margin: 10,
     
    },
    btnStyle:{
        backgroundColor: '#0f5652',
        height: 28,
        width: 150,
        borderRadius: 2,
        marginLeft: 204,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
       
    },
    textStyle: {
        color: '#fff',
        fontWeight: '800'
    },
    tabStyle: {
        backgroundColor: '#0f5652',
        height:15
    }
});