import React, {Component} from 'react'
import { Container,
    Header,
    Content,
    Footer,
    FooterTab,
    Button,
    Icon,
    Left,
    Body,
    Right, Form, Textarea,
    Tabs, Tab, TabHeading
 } from 'native-base';
 import {Router, Scene, Actions} from 'react-native-router-flux';

class AppFooter extends Component {
    constructor(){
        super();
        this.state={
            activeTabName: 'chat'
        };
    }
    tabAction(tab){
        this.setState({activeTabName: tab})
        if(tab === 'chat'){
            Actions.chat();
        }
        else if (tab === 'home'){
            Actions.home(); 
        }
        else{
            Actions.profile();
        }
    }
    render() { 
        return ( 
            <Footer>
            <FooterTab>
                <Button
                    active={(this.state.activeTabName === 'home')? true: ""}
                    onPress={()=>{this.tabAction('home')}}
                    vertical>
                    <Icon name="home" />
                </Button>
                <Button 
                    active={(this.state.activeTabName=== 'chat')? true: ""}
                    onPress={()=>{this.tabAction('chat')}}
                    vertical>
                    <Icon name="chatbubbles" />
                </Button>
                <Button 
                    active={(this.state.activeTabName=== 'profile')? true: ""}
                    onPress={()=>{this.tabAction('profile')}}
                    vertical>
                    <Icon name="person" />
                </Button>
            </FooterTab>
        </Footer>
         );
    }
}
 
export default AppFooter;