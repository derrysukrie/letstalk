import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';
import { Container,
    Content,
    Button,
    Form, Textarea,
 } from 'native-base';
import AppHeader from '../../components/header/AppHeader';
import axios from 'axios';
import AppTab from '../../components/tabs/AppTab';


export default class SharingTalk extends Component {
    constructor(props){
        super(props);
        this.state = {
            title: '',
            categories: '',
            content: ''
        }
    }

    changedTitle = event => {this.setState({title: event.target.value})};

    submitHandle = event => {
        event.preventDefault();

        axios.post(`http://reduxblog.herokuapp.com/api/posts?key=derry`,
        {title: this.state.title, categories: this.state.categories, content: this.state.content })
        .then(res => {
            console.log(res.data);
            alert("Status Was Updated")
            this.setState({ title: res.data });
            this.props.history.push('chat'); //baru
        })
    }

    render() {
        console.log('ini inin', this.state.title)
        return (
            
            <Container style={styles.color}>
                <AppHeader />
                <Content>
                
                <Form>
                    <Textarea 
                    onChangeText={(title) => this.setState({title})}
                    rowSpan={4}
                    bordered 
                    placeholder="Write your story..."
                    style={styles.textInput1}
                    />
                    <Button onPress={this.submitHandle} style={styles.btnStyle}>
                        <Text style={styles.textStyle}>Share</Text>
                    </Button>
                </Form>
               
               <AppTab />
              
            </Content>
            
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    color: {
        backgroundColor: '#97cdd0'
    },
    listMenuColor: {
        color: 'black'
    },
    colors:{
        color: '#fff'
    },
    textInput1: {
        borderRadius: 5,
        backgroundColor: '#fff',
        marginTop: 10,
        margin: 10,
        paddingTop: 10
    },
    textInput2: {
        borderRadius: 3,
        backgroundColor: '#fff',
        fontSize: 11,
        margin: 10,
     
    },
    btnStyle:{
        backgroundColor: '#0f5652',
        height: 28,
        width: 150,
        borderRadius: 7,
        marginLeft: 204,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 25
       
    },
    textStyle: {
        color: '#fff',
        fontWeight: '800'
    },
    tabStyle: {
        backgroundColor: '#0f5652',
        height:15
    }
});