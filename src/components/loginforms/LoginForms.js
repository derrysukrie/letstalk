import React, { Component } from 'react';
import {Container, Form, Item, Label, Content, Input} from 'native-base';

class LoginForms extends Component {
    render() { 
        return ( 
            <Container>
                <Content>
                <Form>
                    <Item stackedLabel>
                    <Label style={{fontSize:12}}>Username</Label>
                    <Input />
                    </Item>
                    <Item stackedLabel>
                    <Label style={{fontSize:12}}>Password</Label>
                    <Input />
                    </Item>
                </Form>
                </Content>
            </Container>
         );
    }
}
 
export default LoginForms;