import React from "react";
import { View } from "react-native";
import {iOSUIKit} from 'react-native-typography';
import {
  Card,
  CardItem,
  Body,
  Text,
  Button,
  Spinner,
  Content
} from "native-base";
import axios from "axios"
import { Actions } from 'react-native-router-flux';

export default class SharingCard extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      statuss: [],
      isLoading: true
    }
  }
  componentDidMount(){
    axios.get(`http://reduxblog.herokuapp.com/api/posts?key=derry`)
      .then(res => {
        this.setState({
          statuss:res.data,
          isLoading: false
        })
      })
  }
  render() {
    console.log('ini data dari server', this.state.statuss)
    console.log('test', this.props.det)
    let spin;
      if(this.state.isLoading){
        spin = <Spinner color="white" />
      } else {
        spin = <Content>
                  {
          this.state.statuss.map((status, index)=>
          <Card key={index}>
            <CardItem
              style={{
                shadowOffset: { width: 6, height: 6 },
                shadowColor: "gray",
                shadowOpacity: 0.3
              }}
            >
              <Body style={{ fontFamily: "Roboto" }}>
                <Text style={{ color: "#0F5652", fontSize: 10, paddingBottom: 6 }}>
                  - Anonymous
                </Text>
                <Text
                  style={{
                    paddingBottom: 15,
                    opacity: 0.7,
                    fontSize: 12
                  }}
                >
                  {status.title}
                </Text>
              </Body>
              <Button onPress= {() => {Actions.detail({det:status});}}><Text>Detail</Text></Button>
            </CardItem>
          </Card>
          )
        }
        </Content>
      }
    return (
      <View>
        {spin}
      </View>
    );
  }
}
