import React, {Component} from 'react';
import Profile from './src/screen/mainscreen/Profile';
import { Container, Button, Footer, FooterTab, Icon } from 'native-base';
import {Router, Scene, Actions} from 'react-native-router-flux';
import SharingTalk from './src/screen/mainscreen/SharingTalk';
import Home from './src/screen/mainscreen/Home';
import AppFooter from './src/components/footer/AppFooter';
import StatusDetail from './src/components/cards/StatusDetail';

class AppRouter extends Component {
    render() { 
        return ( 
            <Container>
                 <Router>
                <Scene 
                    key="root">
                    <Scene 
                        key='chat' component={SharingTalk}
                        hideNavBar={true}
                    />
                    <Scene 
                        key='home' component={Home}
                        hideNavBar={true}    
                    />
                     <Scene 
                        key='profile' component={Profile}
                        hideNavBar={true}
                    />
                    <Scene
                        key='detail' component={StatusDetail}
                        hideNavBar={true}
                    />
                </Scene>
                </Router>
               <AppFooter />
            </Container>
         );
    }
}
 
export default AppRouter;