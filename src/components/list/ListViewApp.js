import React, { Component } from 'react';
import { TouchableOpacity, ScrollView } from 'react-native';
import {List, ListItem, Text, Left, Body, Container, Content, View} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';

class ListViewApp extends Component {
    constructor(props){
        super(props)
        this.state = {
            statusUser : [],
            isLoading: true

        }
    }
    componentDidMount(){
        axios.get(`http://reduxblog.herokuapp.com/api/posts?key=derry`)
        .then(res => {
            this.setState({
                statusUser: res.data,
                isLoading: true
            })
        })
    }
    render() { 
        // console.log('ini data dari sever',this.state.statusUser)
        return ( 
            <Content>
            {
                this.state.statusUser.map((statuss, index)=>
                <List key={index}>
                    <ListItem ScrollView={ScrollView}>
                    <Body>
                        <Text style={{bottom:5, fontSize:13}}>Anonymous</Text>
                        <Text note>{statuss.title}</Text>
                        <View style={{flex:1}}>
                        <TouchableOpacity>
                            <Icon name='comment'
                                size={15}
                                style={{left:10, top:5, color:'#97cdd0'}}
                                >                   
                            </Icon> 
                        </TouchableOpacity>
                        </View>
                    </Body>
                    </ListItem>
                </List>
                )
            }
        </Content>
         );
    }
}
 
export default ListViewApp ;