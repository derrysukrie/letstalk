import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Content, Title, Text } from 'native-base';
import { Actions } from 'react-native-router-flux'

class StatusDetail extends Component {
    constructor(props) {
        super(props)
    }
    render(){
        console.log('ini data detail', this.props.det)
        let statusdet = this.props.det
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress= {() => {Actions.pop(); }} transparent>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body><Title>Detail</Title></Body>
                    <Right />
                </Header>
                <Content>
                    <Text>{statusdet.title}</Text>
                    <Text>{statusdet.categories}</Text>
                </Content>
            </Container>
        )
    }
}
 
export default StatusDetail;