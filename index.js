/** @format */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import AppRouter from './AppRouter';
import Login from './src/screen/login/Login.js';


AppRegistry.registerComponent(appName, () => Login);
